﻿using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Runtime.InteropServices;
using System.Collections.Specialized;
using UnityEngine.SceneManagement; //Gives more access over the scene, helps call different scenes
using System.Security.Cryptography;

// Include the namespace required to use Unity UI and Input System

public class PlayerController : MonoBehaviour {
	
	// Create public variables for player speed, and for the Text UI game objects
	public float speed = 0;
	public float jumpPower; //Gives a editing function in Unity to be able to manually change the jump power
	public TextMeshProUGUI countText;
	    
	public GameObject winTextObject;

        private float movementX;
        private float movementY;

	private Rigidbody rb;
	private int count;

	// At the start of the game..
	void Start ()
	{
		// Assign the Rigidbody component to our private rb variable
		rb = GetComponent<Rigidbody>();

		// Set the count to zero 
		count = 0;

		SetCountText ();

                // Set the text property of the Win Text UI to an empty string, making the 'You Win' (game over message) blank
        winTextObject.SetActive(false);
	}

	private void Update()
    {
		if (transform.position.y < -10f) //When the player reaches a why of -10 the level will restart. 
        {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			//When the if condition is met this will activate and restart the level. 
        }
    }

	void FixedUpdate ()
	{
		// Create a Vector3 variable, and assign X and Z to feature the horizontal and vertical float variables above
		Vector3 movement = new Vector3 (movementX, 0.0f, movementY);

		rb.AddForce (movement * speed);
	}

	void OnTriggerEnter(Collider other) 
	{
		// ..and if the GameObject you intersect has the tag 'Pick Up' assigned to it..
		if (other.gameObject.CompareTag ("PickUp"))
		{
			other.gameObject.SetActive (false);

			// Add one to the score variable 'count'
			count = count + 1;

			// Run the 'SetCountText()' function (see below)
			SetCountText ();
		}

		//This gameobject is connected to the second PickUp
		if (other.gameObject.CompareTag("PickUpTwo"))
        {
			other.gameObject.SetActive(false);
			count = count + 3; //Add three to the score variable

			SetCountText(); // displays an updated count in the SetCountText 
        }
	}

        void OnMove(InputValue value)
        {
        	Vector2 v = value.Get<Vector2>();

        	movementX = v.x;
        	movementY = v.y;
        }

	void OnJump()
    {
		Jump(); //Calls the jump funtion 
    }
	void Jump()
    {
		rb.AddForce(Vector3.up * jumpPower); //Makes the ball effect the Y, so that it will move up and down
		print("Still Jumping!)"); // Print statement to make sure action is actually occuring

    }


        void SetCountText()
	{
		countText.text = "Count: " + count.ToString();

		if (count >= 40) //Raised the win game count, The player must now get 20 points in order to win the game.
		{
                    // Set the text value of your 'winText'
                    winTextObject.SetActive(true);
		}
	}
}


